<?php
/* 
 * Aptify SSO log-in integration
 * by James Hickman <jhickman@arcdigitalservices.com>
 * 
 * Simple end-point for the Aptify SSO web service.
 * 
 * User profiles need to have a numeric field named 'aptify_id'
 * and a user group names 'Members' must exist.
 */
include_once('classes/aptify_sso.class.php');

// The ID code of this regional society.
const ORG_REMOTE_LOGIN_ID = ''; // ID Provided by Aptify

function _aptify_login() {
    // Implement Aptify log-in operation here
    $epid = $_GET['EPID'];
    $redirectTo = $_GET['OrgTargetURL'];
    $api = new AptifySSO();
    $userinfo = $api->fetchuser($epid);
    
    if($userinfo === FALSE || $userinfo == null) die("Failed to retrieve information from Aptify.");
    
    $aptify_id = (int)$userinfo->Person->PersonID;
    $email = (string)$userinfo->Person->PrimaryEmail;

    // Query database for the Aptify user ID in the field_revision_field_aptify_id table.
    // If found the entity_id is the user we're looking for.
    $theuser = FALSE;
    $result = db_query("SELECT entity_id FROM {field_revision_field_aptify_id} WHERE field_aptify_id_value = :aptify_id", array(':aptify_id' => $aptify_id));
    if ($result->rowCount() > 0) {
        $row = $result->fetchAssoc();
        $user_id = (int)$row['entity_id'];
        $theuser = user_load($user_id);
    }
    
    // Try by user name
    if ($theuser === FALSE) {
        $theuser = user_load_by_name((string)$userinfo->Person->FirstLast);
    }

    // Else search the user by the Email address
    if ($theuser === FALSE) {
        $theuser = user_load_by_mail($email);
    }

    // Else create a new User record.
    if ($theuser === FALSE) {
        $theuser = array(
            'name' => (string)$userinfo->Person->FirstLast,
            'pass' => md5($email . (string)rand()),
            'mail' => $email,
            'signature_format' => 'full_html',
            'status' => 1,
            'timezone' => 'America/Los_Angeles',
            'init' => 'Email',
            'roles' => array(
                DRUPAL_AUTHENTICATED_RID => 'authenticated user'
            ),
            'field_aptify_id' => array(
                'und' => array(
                    array(
                        'value' => $aptify_id
                    )
                )
            )
        );
        $theuser = user_save('', $theuser);
    }
        
    // Need to get the ID of the 'Member' Role.
    $result = db_query("SELECT rid FROM {role} WHERE name = 'Member'");
    if ($result->rowCount() > 0) {
        $row = $result->fetchAssoc();
        $member_group_id = (int)$row['rid'];
    }

    // Update the user's data object from the Aptify data.
    $theuser->name = (string)$userinfo->Person->FirstLast . ' ' . (string)$userinfo->Person->PersonID; // Need to be really sure the Drupal username is unique.
    $theuser->mail = (string)$userinfo->Person->PrimaryEmail;
    $theuser->field_aptify_id['und'][0]['value'] = (string)$userinfo->Person->PersonID;
    
    /*
     * Example of translating the group IDs returned from the Aptify API to native Drupal roles.
     * The roles are numerical ID codes so there needs to be mappin logic of some sort.
     * In this case I hard-coded the tests.
     */
    // If the user is a valid member of this regional association add the Member role, else remove from Member.
    /*if ((string)$userinfo->Person->ComponentID == ORG_REMOTE_LOGIN_ID && ((string)$userinfo->Person->MemberStatusID == '12' || (string)$userinfo->Person->MemberStatusID == '14')) {
        $theuser->roles[$member_group_id] = 'Member';
    }
    else {
        unset($theuser->roles[$member_group_id]);
    }
    */

    // Save the User to the database
    user_save($theuser);
    
    // Set the current session to be the user.
    global $user;
    $user = $theuser;
    drupal_session_regenerate();

    // Redirect to the target URL.
    // Default to the Home page if no redirect provided.
    if (strlen($redirectTo) == 0) {
        drupal_goto('/');
    }
    else {
        drupal_goto($redirectTo);
    }
}

function _aptify_logout() {
    session_destroy();
    AptifySSO::logout();
}