<?php
/* 
 * Simple class to interact with the Aptify SSO interface.
 * by James Hickman <jhickman@arcdigitalservices.com>
 * 
 * If $testing = TRUE passed to the Constructor a dummy example XML
 * string us ised rather then interrigating the Aptify SSO Server.
 */
class AptifySSO {
    /*
     * URLs and access codes would need to be provided by
     * Aptify.
     * 
     * http://www.aptify.com/
     */
    const SERVICE_URL = "";
    const LOGOUT_URL = "";
    const API_KEY = "";
    function __construct($testing = FALSE) {
        $this->testmode = $testing;
        if (!$testing) {
            // Set up the SOAP interface
            $this->apiClient = new SoapClient(AptifySSO::SERVICE_URL . "?WSDL");
        }
    }
    function fetchuser($secret) {
        if ($this->testmode) {
            // Return testing data for off-line development.
            $txml = "<?xml version=\"1.0\" encoding=\"utf-16\" standalone=\"yes\"?>"
                    . "<ADAPersonInfo>"
                    . " <Person>"
                    . "  <PersonID>999</PersonID>"
                    . "  <FirstName>New</FirstName>"
                    . "  <LastName>User</LastName>"
                    . "  <ADANumber>1234567890</ADANumber>"
                    . "  <PrimaryFunction>a</PrimaryFunction>"
                    . "  <PrimaryFunctionID>b</PrimaryFunctionID>"
                    . "  <MemberStatus>c</MemberStatus>"
                    . "  <MemberStatusID>14</MemberStatusID>"
                    . "  <ComponentName>e</ComponentName>"
                    . "  <ComponentID>461</ComponentID>"
                    . "  <ComponentJurisdiction></ComponentJurisdiction>"
                    . "  <ConstituentName>g</ConstituentName>"
                    . "  <ConstituentID>h</ConstituentID>"
                    . "  <ConstituentJurisdiction></ConstituentJurisdiction>"
                    . "  <PrimaryEmail>newuser@host.com</PrimaryEmail>"
                    . "  <Birthday>i</Birthday>"
                    . "  <Gender>Male</Gender>"
                    . "  <PracticeTypeID>j</PracticeTypeID>"
                    . "  <PracticeTypeName>k</PracticeTypeName>"
                    . "  <LabelName>newuser</LabelName>"
                    . "  <FirstLast>New User</FirstLast>"
                    . " </Person>"
                    . "</ADAPersonInfo>";
            $sxe = simplexml_load_string($this->cleanXMLstring($txml));
            libxml_use_internal_errors(FALSE);
            if ($sxe === false) {
            echo "Failed loading XML\n";
                foreach(libxml_get_errors() as $error) {
                    echo "\t", $error->message;
                }
            }
            return $sxe;
        }
        else {
            // Do the real request.
            $requestParams = array(
                'APICode' => AptifySSO::API_KEY,
                'EnPID' => $secret
            );
            $result = $this->apiClient->GetPersonInfo($requestParams);
            $xml = (string)$result->GetPersonInfoResult;
            return simplexml_load_string($this->cleanXMLstring($xml));
        }
    }
    
    function cleanXMLstring($xml) {
        return str_replace("<?xml version=\"1.0\" encoding=\"utf-16\" standalone=\"yes\"?>", '', $xml);
    }
    
    static function logout($dest = '') {
        drupal_goto(AptifySSO::LOGOUT_URL . 'http://' . $_SERVER['SERVER_NAME'] . '/' . $dest);
    }
}
?>